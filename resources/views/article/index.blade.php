<html>
  <head>
    <link href="public/css/style.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
    <link href="public/css/style.css" rel="stylesheet" type="text/css">
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <link href="public/css/style.css" rel="stylesheet" type="text/css">
  </head>
  <body background="img/pattern.jpg" class="ml-5 mr-5">
    <header>
    </header>
<body>
    <div class="container">
      @foreach ($index_user as $item)
        <div class="card md-4 mt-4 mr-4" style="width: 15rem;">
            <div class="card-body">
                <h5 class="card-title"><P>{{$item->name}}</p></h5>
                <h6 class="card-subtitle mb-2 text-muted"> {{$item->name_with_namespace}}</h6>
                <p class="card-text"></p>
                <a href="{{$item->web_url}}" class="card-link">Open to Gitlab</a><br>
            </div>
        </div>
        @endforeach 
    </div>    
    <footer class="bg-dark text-white md-3 mt-3 mr-3">
            <div class="container">
                <div class="row text-center pt-3">
                    <div class="col">
                        <h5>copyright &copy; 2019 by Sanbercode</h5>
                    </div>
                </div>
            </div>
        </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>
</html>
