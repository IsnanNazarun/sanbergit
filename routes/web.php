<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/article/index','HomeController@index');
Route::get('/article/index1','HomeController@index1');
Route::get('/article/index2','HomeController@index2');
Route::get('/article/home','HomeController@home');
Route::post('/article/newp','HomeController@newp');
